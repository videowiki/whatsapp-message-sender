const amqp = require('amqplib/callback_api')
require('dotenv').config()
const { login, sendTextMessage, sendMediaMessage } = require('vw-turn.io')
const queue = 'whatsapp_bot_message_queue'
const RABBITMQ_SERVER = process.env.RABBITMQ_SERVER

login(process.env.TURNIO_USERNAME, process.env.TURNIO_PASSWORD)
  .then(response => {
    amqp.connect(RABBITMQ_SERVER, (error0, connection) => {
      if (error0) {
        throw error0
      }
      connection.createChannel((error1, channel) => {
        if (error1) {
          throw error1
        }
        channel.prefetch(1)
        channel.assertQueue(queue, {
          durable: true
        })
        channel.consume(
          queue,
          msg => {
            var responseMsg = JSON.parse(msg.content.toString())
            if (responseMsg.type === 'text') {
              sendTextMessage(responseMsg.text, responseMsg.to)
                .then(response => {
                  channel.ack(msg)
                })
                .catch(error => {
                  channel.ack(msg)
                  console.log(error)
                })
            } else if (responseMsg.type === 'voice') {
              sendTextMessage(responseMsg.text, responseMsg.to)
                .then(response => {
                  sendMediaMessage(responseMsg.audioId, responseMsg.to)
                    .then(response => {
                      channel.ack(msg)
                    })
                    .catch(error => {
                      channel.ack(msg)
                      console.log(error)
                    })
                })
                .catch(error => {
                  channel.ack(msg)
                  console.log(error)
                })
            }
          },
          {
            noAck: false
          }
        )
      })
    })
  })
  .catch(error => console.log(error))
